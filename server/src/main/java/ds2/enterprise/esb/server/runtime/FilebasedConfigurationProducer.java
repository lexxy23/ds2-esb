package ds2.enterprise.esb.server.runtime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.client.api.dto.EsbServerConfigurationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import java.lang.invoke.MethodHandles;

@Dependent
public class FilebasedConfigurationProducer {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Produces
    @ApplicationScoped
    public EsbServerConfiguration produceConfig() {
        LOG.debug("Creating dummy config..");
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        DeserializationProblemHandler myHandler = new MyJacksonHandler();
        mapper.addHandler(myHandler);
        EsbServerConfigurationDto rc = null;
        String sysPropConfig = System.getProperty("ds2.esb.configfile");
        LOG.info("Trying to read config from {}", sysPropConfig);
        try {
            rc = mapper.readValue(getClass().getResourceAsStream(sysPropConfig), EsbServerConfigurationDto.class);
            //System.out.println(ReflectionToStringBuilder.toString(user, ToStringStyle.MULTI_LINE_STYLE));
        } catch (Exception e) {
            LOG.error("Error when reading the server config via {}!", sysPropConfig, e);
        }
        return rc;
    }

    private class MyJacksonHandler extends DeserializationProblemHandler {

    }
}
