package ds2.enterprise.esb.server.runtime;

import ds2.enterprise.esb.client.api.EsbServerException;
import ds2.enterprise.esb.impl.EsbServer;
import ds2.enterprise.esb.server.api.ByteBufferGenerator;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by dstrauss on 17.02.17.
 */
public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        Weld weld = new Weld().addPackages(true, EsbServer.class.getPackage()).addPackages(true, ByteBufferGenerator.class.getPackage()).addPackages(true, Main.class.getPackage());
        try {
            LOG.info("Initializing environment..");
            try (WeldContainer wc = weld.initialize()) {
                EsbServer s = wc.select(EsbServer.class).get();
                s.prepare();
                s.startServer();
                LOG.info("Server is up and running. Press any key to exit..");
                if (System.console() != null) {
                    System.console().readLine();
                } else {
                    try {
                        System.in.read();
                    } catch (IOException e) {
                    }
                }

            } catch (EsbServerException e) {
                LOG.error("Error when setting up the server!", e);
            }
        } finally {
            LOG.debug("Done with main method.");
        }
    }
}
