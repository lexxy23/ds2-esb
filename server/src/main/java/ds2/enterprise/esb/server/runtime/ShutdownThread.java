package ds2.enterprise.esb.server.runtime;

import ds2.enterprise.esb.impl.EsbServer;

/**
 * Created by dstrauss on 21.02.17.
 */
public class ShutdownThread extends Thread {
    private EsbServer s;

    public ShutdownThread(EsbServer s) {
        super("EsbServerShutdownThread");
        this.s = s;
    }

    @Override
    public void run() {
        s.stopServer();
    }
}
