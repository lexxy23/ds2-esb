package ds2.enterprise.esb.server.runtime;

import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.client.api.EsbServerConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import java.lang.invoke.MethodHandles;

/**
 * Created by dstrauss on 17.02.17.
 */
@Dependent
public class DummyProducer {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

//    @Produces
//    @ApplicationScoped
    public EsbServerConfiguration produceConfig() {
        LOG.debug("Creating dummy config..");
        EsbServerConfiguration rc = EsbServerConfigurationBuilder.startWith().createHttpForwardConfiguration().listenOn(8082).forwardTo("localhost", 8080, "/master").startsWith("/master").endThisConfig().build();
        return rc;
    }
}
