package ds2.enterprise.esb.client.api.tests;

import ds2.enterprise.esb.client.api.EsbServerConfigurationBuilder;
import ds2.enterprise.esb.client.api.dto.EsbServerConfigurationDto;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by dstrauss on 17.02.17.
 */
public class EsbConfigBuilderTest {
    @Test
    public void createForwardConfig() {
        EsbServerConfigurationDto config = EsbServerConfigurationBuilder.startWith().createHttpForwardConfiguration().listenOn(80).forwardTo("localhost", 80, "/master").startsWith("/master/").endThisConfig().build();
        Assert.assertNotNull(config);
        Assert.assertNotNull(config.getRules());
        Assert.assertNotNull(config.getRules().get(0));
        Assert.assertEquals(config.getRules().get(0).getType(), FlowTypes.HTTP);
    }

    @Test
    public void createForward2Config() {
        EsbServerConfigurationDto config = EsbServerConfigurationBuilder.startWith().createHttpForwardConfiguration().listenOn(80).forwardTo("localhost", 80, "/master").startsWith("/master/").endThisConfig()
                .createHttpForwardConfiguration()
                .listenOn(80).startsWith("/medical").forwardTo("localhost", 82, "/master").endThisConfig()
                .build();
        Assert.assertNotNull(config);
        Assert.assertNotNull(config.getRules());
        Assert.assertNotNull(config.getRules().get(0));
        Assert.assertEquals(config.getRules().get(0).getType(), FlowTypes.HTTP);
    }
}
