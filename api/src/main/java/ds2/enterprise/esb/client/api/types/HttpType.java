package ds2.enterprise.esb.client.api.types;

import ds2.enterprise.esb.client.api.FlowType;

/**
 * As long as we cannot make use of enums in generics, we use these helper type.
 */
public class HttpType implements FlowType {
    public int getTypeId() {
        return FlowTypes.HTTP.getTypeId();
    }
}
