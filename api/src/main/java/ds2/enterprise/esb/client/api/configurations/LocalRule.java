package ds2.enterprise.esb.client.api.configurations;

import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.types.LocalFlowType;

import java.nio.file.Path;

public interface LocalRule extends EsbRule<LocalFlowType> {
    String getRequestPath();

    String getLocalPath();

    Path getAsPath();
}
