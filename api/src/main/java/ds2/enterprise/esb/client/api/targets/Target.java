package ds2.enterprise.esb.client.api.targets;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.client.api.dto.HttpTargetDto;

/**
 * Created by dstrauss on 17.02.17.
 */
@JsonSubTypes({
        @JsonSubTypes.Type(value = HttpTargetDto.class, name = "http"),
})
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "protocol")
public interface Target<E extends FlowType> {
    String getProtocol();
    
}
