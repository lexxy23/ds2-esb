package ds2.enterprise.esb.client.api;

import ds2.enterprise.esb.client.api.connectors.Connector;

import java.util.List;
import java.util.Set;

/**
 * Created by dstrauss on 17.02.17.
 */
public interface EsbServerConfiguration {
    String getName();

    String getCallIdHeaderName();

    List<EsbRule<? extends FlowType>> getRules();

    List<Connector<? extends FlowType>> getConnectors();

    int getSocketIdleSeconds();

    int getExecutorCount();

    long getShutdownExecutorTimeout();

    int getReadTimeout();

    int getWriteTimeout();

    List<String> getFilters();

    String getServerId();
}
