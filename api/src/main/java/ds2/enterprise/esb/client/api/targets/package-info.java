/**
 * This package defines the contracts to access internal server resources, like other web servers, mail servers etc.
 */
package ds2.enterprise.esb.client.api.targets;