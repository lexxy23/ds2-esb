package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.connectors.HttpConnector;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import lombok.*;

/**
 * Created by dstrauss on 17.02.17.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HttpConnectorDto implements HttpConnector {
    private String name;
    private String networkInterface;
    private int listenPort;

    public FlowTypes getType() {
        return FlowTypes.HTTP;
    }

}
