package ds2.enterprise.esb.client.api.connectors;

import ds2.enterprise.esb.client.api.types.HttpType;

/**
 * Created by dstrauss on 02.03.17.
 */
public interface HttpConnector extends Connector<HttpType> {
    String getNetworkInterface();

    int getListenPort();
}
