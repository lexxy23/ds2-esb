package ds2.enterprise.esb.client.api.targets;

import ds2.enterprise.esb.client.api.types.HttpType;

/**
 * Created by dstrauss on 01.03.17.
 */
public interface HttpTarget extends Target<HttpType> {
    String getTargetHost();

    int getTargetPort();
}
