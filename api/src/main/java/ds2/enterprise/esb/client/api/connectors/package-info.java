/**
 * This package defines the types that the server can listen on. Typically this contains the connector logic to setup
 * some listen ports.
 */
package ds2.enterprise.esb.client.api.connectors;