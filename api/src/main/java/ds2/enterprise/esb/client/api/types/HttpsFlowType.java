package ds2.enterprise.esb.client.api.types;

import ds2.enterprise.esb.client.api.FlowType;

public class HttpsFlowType implements FlowType {
    @Override
    public int getTypeId() {
        return FlowTypes.HTTPS.getTypeId();
    }
}
