package ds2.enterprise.esb.client.api.connectors;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.client.api.dto.HttpConnectorDto;
import ds2.enterprise.esb.client.api.types.FlowTypes;

/**
 * Created by dstrauss on 17.02.17.
 */
@JsonSubTypes({
        @JsonSubTypes.Type(value = HttpConnectorDto.class, name = "http"),
})
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
public interface Connector<E extends FlowType> {
    String getName();

    FlowTypes getType();
}
