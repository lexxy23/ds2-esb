package ds2.enterprise.esb.client.api;

/**
 * Created by dstrauss on 01.03.17.
 */
public enum EsbErrorCode {
    UNKNOWN(1), UNKNOWN_REQUEST_TYPE(2), NO_CONFIG_MATCH(3), INTERNAL_ERROR(4);
    private int errorId;

    EsbErrorCode(int id) {
        errorId = id;
    }

    public int getErrorId() {
        return errorId;
    }
}
