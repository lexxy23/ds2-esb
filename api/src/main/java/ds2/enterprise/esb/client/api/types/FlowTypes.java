package ds2.enterprise.esb.client.api.types;

import ds2.enterprise.esb.client.api.FlowType;

/**
 * Created by dstrauss on 17.02.17.
 */
public enum FlowTypes implements FlowType {
    HTTP(1), HTTPS(2), LOCAL(3);

    FlowTypes(int i) {
        id = i;
    }

    int id;

    public int getTypeId() {
        return id;
    }

    public static FlowTypes valueById(int typeId) {
        for (FlowTypes f : values()) {
            if (f.id == typeId) {
                return f;
            }
        }
        return null;
    }
}
