package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.HttpMethod;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.server.api.HttpServerRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.ByteArrayInputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by dstrauss on 27.02.17.
 */
@Setter
@Getter
@ToString(exclude = {"payload"})
@NoArgsConstructor
public class HttpWebRequestDto implements HttpServerRequest {
    private String requestIpAddress;
    private Map<String, List<String>> queryParameters;
    private Map<String, String> headers;
    private ByteArrayInputStream payload;
    private HttpMethod method;
    private String externalCallId;
    private String callId;
    private LocalDateTime requestStartTime;
    private String requestPath;
    private Map<String, String> requestHeaders;
    private Map<String, List<String>> requestParameters;

    @Override
    public FlowTypes getType() {
        return FlowTypes.HTTP;
    }

    @Override
    public boolean hasPayload() {
        return getPayloadSize() > 0;
    }

    @Override
    public long getPayloadSize() {
        return Long.valueOf(headers.get("Content-Length"));
    }
}
