package ds2.enterprise.esb.client.api.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.client.api.connectors.Connector;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstrauss on 17.02.17.
 */
@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE
)
public class EsbServerConfigurationDto implements EsbServerConfiguration {
    private List<EsbRule<? extends FlowType>> rules = new ArrayList<>();
    private List<Connector<? extends FlowType>> connectors;
    private List<String> filters;
    private int socketIdleSeconds = 10;
    private long shutdownExecutorTimeout = 10;
    private int executorCount = 10;
    private int readTimeout = 30000;
    private int writeTimeout = 20000;
    private String name;
    private String serverId;
    private String callIdHeaderName;

}
