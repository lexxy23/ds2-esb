package ds2.enterprise.esb.client.api.configurations;

import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.client.api.connectors.Connector;
import ds2.enterprise.esb.client.api.targets.Target;

/**
 * Created by dstrauss on 17.02.17.
 */
public interface ForwardRule<E extends FlowType> extends EsbRule<E> {
    Connector<E> getListener();

    Target<E> getTarget();

    String getRequestPath();
}
