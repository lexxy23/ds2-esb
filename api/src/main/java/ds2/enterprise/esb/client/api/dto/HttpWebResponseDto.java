package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.types.HttpType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

/**
 * Created by dstrauss on 27.02.17.
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
public class HttpWebResponseDto extends AbstractEsbServerResponseDto<HttpType> {
    private Map<String, String> headers;

}
