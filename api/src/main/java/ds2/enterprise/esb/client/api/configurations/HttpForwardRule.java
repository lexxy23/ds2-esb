package ds2.enterprise.esb.client.api.configurations;

import ds2.enterprise.esb.client.api.types.HttpType;

/**
 * Created by dstrauss on 02.03.17.
 */
public interface HttpForwardRule extends ForwardRule<HttpType> {
}
