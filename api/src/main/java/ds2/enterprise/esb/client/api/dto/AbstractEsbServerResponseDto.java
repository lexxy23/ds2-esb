package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.server.api.EsbServerResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.nio.ByteBuffer;
import java.time.LocalDateTime;

/**
 * Created by dstrauss on 28.02.17.
 */
@ToString
@Getter
@Setter
public class AbstractEsbServerResponseDto<E extends FlowType> implements EsbServerResponse<E> {
    private ByteBuffer payload;
    private String callId;
    private int resultCode;
    private LocalDateTime responseCreatedTime;

    @Override
    public long getPayloadLength() {
        long length = payload.limit();
        return length;
    }
}
