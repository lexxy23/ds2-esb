package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.FlowType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by dstrauss on 17.02.17.
 */
@Setter
@Getter
public abstract class AbstractEsbRuleDto<E extends FlowType> implements EsbRule<E> {
    private String name;
    private FlowType type;
    private List<String> filters;
}
