package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.client.api.configurations.HttpForwardRule;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by dstrauss on 17.02.17.
 */
@ToString
@Setter
@Getter
public class SimpleForwardRule extends AbstractEsbRuleDto<HttpType> implements HttpForwardRule {
    private HttpConnectorDto listener;
    private String requestPath;
    private HttpTargetDto target;

    @Override
    public FlowType getType() {
        return FlowTypes.HTTP;
    }
}
