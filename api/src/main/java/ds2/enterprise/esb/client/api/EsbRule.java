package ds2.enterprise.esb.client.api;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ds2.enterprise.esb.client.api.dto.LocalRuleDto;
import ds2.enterprise.esb.client.api.dto.SimpleForwardRule;

import java.util.List;

/**
 * Created by dstrauss on 17.02.17.
 */
@JsonSubTypes({
        @JsonSubTypes.Type(value = SimpleForwardRule.class, name = "forward"),
        @JsonSubTypes.Type(value = LocalRuleDto.class, name = "local"),
})
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
public interface EsbRule<E extends FlowType> {
    String getName();

    FlowType getType();

    List<String> getFilters();
}
