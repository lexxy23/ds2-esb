package ds2.enterprise.esb.client.api;

import lombok.Getter;

@Getter
public class EsbRequestException extends Exception {
    private RequestErrorCodes errorCode;

    public EsbRequestException(RequestErrorCodes errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
