package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.targets.HttpTarget;
import lombok.*;

/**
 * Created by dstrauss on 17.02.17.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class HttpTargetDto implements HttpTarget {
    private String targetHost;
    private int targetPort;
    private String protocol;

}
