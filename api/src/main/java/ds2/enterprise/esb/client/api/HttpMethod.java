package ds2.enterprise.esb.client.api;

/**
 * Created by dstrauss on 28.02.17.
 */
public enum HttpMethod {
    GET, PUT, POST, HEAD, DELETE, OPTIONS;
}
