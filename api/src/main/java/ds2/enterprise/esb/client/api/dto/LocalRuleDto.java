package ds2.enterprise.esb.client.api.dto;

import ds2.enterprise.esb.client.api.configurations.LocalRule;
import ds2.enterprise.esb.client.api.types.LocalFlowType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.nio.file.Path;
import java.nio.file.Paths;

@Setter
@Getter
@ToString
public class LocalRuleDto extends AbstractEsbRuleDto<LocalFlowType> implements LocalRule {
    private String requestPath;
    private String localPath;

    public Path getAsPath() {
        return Paths.get(localPath);
    }
}
