package ds2.enterprise.esb.client.api;

/**
 * Created by dstrauss on 17.02.17.
 */
public interface FlowType {
    /**
     * Returns the id of the type.
     *
     * @return the type id
     */
    int getTypeId();
}
