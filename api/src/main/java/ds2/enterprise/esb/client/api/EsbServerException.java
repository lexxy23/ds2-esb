package ds2.enterprise.esb.client.api;

/**
 * Created by dstrauss on 27.02.17.
 */
public class EsbServerException extends Exception {
    private EsbErrorCode errorCode;

    public EsbServerException(EsbErrorCode code, String msg) {
        super(msg);
        errorCode = code;
    }

    public EsbServerException(EsbErrorCode code, String msg, Throwable t) {
        super(msg, t);
        errorCode = code;
    }

    public EsbErrorCode getErrorCode() {
        return errorCode;
    }
}
