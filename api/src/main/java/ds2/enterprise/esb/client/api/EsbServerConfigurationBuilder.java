package ds2.enterprise.esb.client.api;

import ds2.enterprise.esb.client.api.dto.EsbServerConfigurationDto;
import ds2.enterprise.esb.client.api.dto.HttpConnectorDto;
import ds2.enterprise.esb.client.api.dto.HttpTargetDto;
import ds2.enterprise.esb.client.api.dto.SimpleForwardRule;

/**
 * Created by dstrauss on 17.02.17.
 */
public class EsbServerConfigurationBuilder {
    private EsbServerConfigurationDto configuration;

    private EsbServerConfigurationBuilder() {
        //nothing to do
        configuration = new EsbServerConfigurationDto();
    }

    public static EsbServerConfigurationBuilder startWith() {
        return new EsbServerConfigurationBuilder();
    }

    public HttpForwardConfigurationBuilder createHttpForwardConfiguration() {
        return new HttpForwardConfigurationBuilder(this);
    }

    public EsbServerConfigurationDto build() {
        return configuration;
    }

    EsbServerConfigurationDto getConfiguration() {
        return configuration;
    }

    /**
     * Contract for all config builders.
     *
     * @param <E> the esb config type
     */
    public abstract class AbstractConfigurationBuilder<E extends EsbRule<?>> {
        private EsbServerConfigurationBuilder parent;
        private E config;

        AbstractConfigurationBuilder(EsbServerConfigurationBuilder parent, E config) {
            this.parent = parent;
            this.config = config;
        }

        public EsbServerConfigurationBuilder endThisConfig() {
            EsbServerConfiguration c = parent.getConfiguration();
            c.getRules().add(config);
            return parent;
        }

        E getThisConfig() {
            return config;
        }
    }

    /**
     * The contract for the simple forward config.
     */
    public class HttpForwardConfigurationBuilder extends AbstractConfigurationBuilder<SimpleForwardRule> {

        HttpForwardConfigurationBuilder(EsbServerConfigurationBuilder p) {
            super(p, new SimpleForwardRule());
        }

        public HttpForwardConfigurationBuilder listenOn(int port) {
            getThisConfig().setListener(new HttpConnectorDto("default 1", "localhost", port));
            return this;
        }

        public HttpForwardConfigurationBuilder forwardTo(String host, int port, String pattern) {
            getThisConfig().setTarget(new HttpTargetDto(host, port, pattern));
            return this;
        }

        public HttpForwardConfigurationBuilder startsWith(String pathStart) {
            getThisConfig().setRequestPath(pathStart);
            return this;
        }

    }
}
