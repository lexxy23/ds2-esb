package ds2.enterprise.esb.server.api;

import java.nio.ByteBuffer;

/**
 * Created by dstrauss on 06.03.17.
 */
public interface ByteBufferGenerator {
    ByteBuffer generate(String plainText);
}
