package ds2.enterprise.esb.server.api;

import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.client.api.types.FlowTypes;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * An incoming server request.
 * Created by dstrauss on 21.02.17.
 */
public interface EsbServerRequest<E extends FlowType> extends Serializable {
    @Deprecated
    FlowTypes getType();

    /**
     * This is the call id that has been received from the external client.
     *
     * @return the external call id
     */
    String getExternalCallId();

    /**
     * This is our internal call id.
     *
     * @return the internal call id
     */
    String getCallId();

    String getRequestIpAddress();

    LocalDateTime getRequestStartTime();

    String getRequestPath();

    Map<String, List<String>> getRequestParameters();

    Map<String,String> getRequestHeaders();
}
