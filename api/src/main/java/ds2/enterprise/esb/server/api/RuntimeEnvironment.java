package ds2.enterprise.esb.server.api;

/**
 * Created by dstrauss on 01.03.17.
 */
public interface RuntimeEnvironment {
    String getVersionString();
    
}
