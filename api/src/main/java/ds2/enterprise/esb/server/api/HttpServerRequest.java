package ds2.enterprise.esb.server.api;

import ds2.enterprise.esb.client.api.HttpMethod;
import ds2.enterprise.esb.client.api.types.HttpType;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by dstrauss on 01.03.17.
 */
public interface HttpServerRequest extends EsbServerRequest<HttpType> {
    /**
     * Returns any query parameters.
     *
     * @return the query parameters
     */
    Map<String, List<String>> getQueryParameters();

    /**
     * Returns any request headers.
     *
     * @return the request headers
     */
    Map<String, String> getHeaders();

    /**
     * Returns the stream to the payload.
     *
     * @return the stream of the payload
     */
    ByteArrayInputStream getPayload();

    /**
     * Returns the http request method.
     *
     * @return the http request method
     */
    HttpMethod getMethod();

    boolean hasPayload();

    /**
     * Returns the payload size, in bytes.
     *
     * @return the payload size, or 0 if no payload is given.
     */
    long getPayloadSize();
}
