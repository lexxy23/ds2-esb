package ds2.enterprise.esb.server.api;

import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.FlowType;

/**
 * Created by dstrauss on 01.03.17.
 */
public interface ConfigurationMatcher<E extends FlowType> {
    EsbRule<E> match(EsbServerRequest<E> req);
}
