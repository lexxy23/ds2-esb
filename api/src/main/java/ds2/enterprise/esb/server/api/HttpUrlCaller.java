package ds2.enterprise.esb.server.api;


import java.net.URL;

/**
 * Created by dstrauss on 02.03.17.
 */
public interface HttpUrlCaller {
    HttpResponse call(URL url, HttpServerRequest incomingRequest);
}
