package ds2.enterprise.esb.server.api;

import ds2.enterprise.esb.client.api.FlowType;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.time.LocalDateTime;

/**
 * Created by dstrauss on 27.02.17.
 */
public interface EsbServerResponse<E extends FlowType> extends Serializable {
    ByteBuffer getPayload();

    long getPayloadLength();

    String getCallId();

    int getResultCode();

    LocalDateTime getResponseCreatedTime();
}
