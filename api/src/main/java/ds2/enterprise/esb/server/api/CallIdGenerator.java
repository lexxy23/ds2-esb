package ds2.enterprise.esb.server.api;

/**
 * Created by dstrauss on 02.03.17.
 */
public interface CallIdGenerator {
    String generate();
}
