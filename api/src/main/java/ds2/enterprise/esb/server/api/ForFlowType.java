package ds2.enterprise.esb.server.api;

import ds2.enterprise.esb.client.api.types.FlowTypes;

/**
 * Created by dstrauss on 27.02.17.
 */
public @interface ForFlowType {
    FlowTypes value();
}
