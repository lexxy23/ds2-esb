package ds2.enterprise.esb.server.api;

import ds2.enterprise.esb.client.api.EsbRequestException;
import ds2.enterprise.esb.client.api.FlowType;

public interface RequestFilter<F extends FlowType, E extends EsbServerRequest<F>> {
    void process(E request) throws EsbRequestException;

    String getFilterId();
}
