package ds2.enterprise.esb.server.api;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface RequestAudit extends Serializable {
    /**
     * The callId for this request.
     * @return
     */
    String getCallId();

    /**
     * the ip address of the client.
     * @return
     */
    String getRequestIpAddress();

    /**
     * The request protocol being used by the client to access this server. Basically, this is the listener protocol.
     * @return
     */
    String getRequestProtocol();

    /**
     * The name of the server that the client used to access this server.
     * @return
     */
    String getRequestServer();

    /**
     * The id of the esb server.
     * @return
     */
    String getEsbServerId();

    /**
     * The internal ip address of the esb server.
     * @return
     */
    String getEsbServerAddress();

    /**
     * The result code of the request.
     * @return
     */
    int getResultCode();

    /**
     * Returns the byte count of the response body.
     * @return
     */
    long getResultSize();

    /**
     * The local datetime when the client request arrived at the server.
     * @return
     */
    LocalDateTime getRequestStartTime();

    /**
     * The duration of the handling of the request within this server.
     * @return
     */
    int getDurationMillis();

    /**
     * The request path. In http, this is the request /bla/test.html etc.
     * @return
     */
    String getRequestPath();
}
