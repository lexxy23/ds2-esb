package ds2.esb.http.request.filters;

import ds2.enterprise.esb.client.api.EsbRequestException;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.server.api.ForFlowType;
import ds2.enterprise.esb.server.api.HttpServerRequest;
import ds2.enterprise.esb.server.api.RequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.lang.invoke.MethodHandles;

@Named("loggingRequestPayloadFilter")
@ApplicationScoped
@ForFlowType(FlowTypes.HTTP)
public class LoggingRequestPayloadFilter implements RequestFilter<HttpType, HttpServerRequest> {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void process(HttpServerRequest request) throws EsbRequestException {
        if (!request.hasPayload()) {
            LOG.debug("This request has no payload! Ignoring..");
            return;
        }
        ByteArrayInputStream thisInputStream = request.getPayload();
    }

    @Override
    public String getFilterId() {
        return "loggingRequestPayloadFilter";
    }
}
