package ds2.esb.http.request.filters;

import ds2.enterprise.esb.client.api.EsbRequestException;
import ds2.enterprise.esb.client.api.RequestErrorCodes;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.server.api.ForFlowType;
import ds2.enterprise.esb.server.api.HttpServerRequest;
import ds2.enterprise.esb.server.api.RequestFilter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named("authenticatedRequestFilter")
@ApplicationScoped
@ForFlowType(FlowTypes.HTTP)
public class AuthenticatedRequestFilter implements RequestFilter<HttpType, HttpServerRequest> {
    @Override
    public void process(HttpServerRequest request) throws EsbRequestException {
        if (!request.getRequestHeaders().containsKey("Authorization")) {
            throw new EsbRequestException(RequestErrorCodes.NOT_AUTHENTICATED, "You are unknown, or not allowed to access this resource!");
        }
    }

    @Override
    public String getFilterId() {
        return "authenticatedRequestFilter";
    }
}
