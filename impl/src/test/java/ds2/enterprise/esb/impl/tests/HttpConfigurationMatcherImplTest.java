package ds2.enterprise.esb.impl.tests;

import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.impl.http.HttpConfigurationMatcherImpl;
import ds2.enterprise.esb.server.api.ConfigurationMatcher;
import ds2.enterprise.esb.server.api.ForFlowType;
import ds2.oss.core.testutils.AbstractInjectionEnvironment;
import org.testng.annotations.BeforeClass;

import javax.inject.Inject;

public class HttpConfigurationMatcherImplTest extends AbstractInjectionEnvironment {
    @Inject
    @ForFlowType(FlowTypes.HTTP)
    private ConfigurationMatcher<HttpType> httpConfigurationMatcher;

    @BeforeClass
    public void onClass() {
        httpConfigurationMatcher = new HttpConfigurationMatcherImpl();
    }
}
