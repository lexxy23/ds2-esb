package ds2.enterprise.esb.impl.tests;

import ds2.enterprise.esb.impl.SimpleByteBufferGeneratorImpl;
import ds2.enterprise.esb.server.api.ByteBufferGenerator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.nio.ByteBuffer;

import static org.testng.Assert.*;

public class ByteBuffer2Test {
    private ByteBufferGenerator bufferGenerator;

    @BeforeClass
    public void onClass() {
        bufferGenerator = new SimpleByteBufferGeneratorImpl();
    }

    @Test
    public void testString1() {
        ByteBuffer buffer = bufferGenerator.generate("test");
        assertNotNull(buffer);
        assertTrue(buffer.hasRemaining());
        assertEquals(buffer.capacity(), 4);
        assertEquals(buffer.limit(), 4);
        assertEquals(buffer.position(), 0);
    }
}
