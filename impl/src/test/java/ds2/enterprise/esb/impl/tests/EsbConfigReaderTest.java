package ds2.enterprise.esb.impl.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import ds2.enterprise.esb.client.api.dto.EsbServerConfigurationDto;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.snakeyaml.engine.v1.api.Load;
import org.snakeyaml.engine.v1.api.LoadSettings;
import org.snakeyaml.engine.v1.api.LoadSettingsBuilder;
import org.snakeyaml.engine.v1.api.lowlevel.Parse;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.yaml.snakeyaml.Yaml;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

public class EsbConfigReaderTest {
    private Load load;
    private Parse parse;

    @BeforeClass
    public void onClass() {
        LoadSettings settings = new LoadSettingsBuilder().setLabel("Custom user configuration").build();
        load = new Load(settings);
        parse = new Parse(settings);
    }

    @Test
    public void testReadYaml12() {
        Object object = load.loadFromInputStream(getClass().getResourceAsStream("/testConfig.yaml"));
        assertNotNull(object);
        //assertTrue(object instanceof EsbServerConfigurationDto);
    }
    @Test
    public void testReadYaml11() {
        Yaml yaml=new Yaml();
        Object object = load.loadFromInputStream(getClass().getResourceAsStream("/testConfig.yaml"));
        assertNotNull(object);
        //assertTrue(object instanceof EsbServerConfigurationDto);
    }

    @Test
    public void testReadJackson() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        DeserializationProblemHandler myHandler=new MyJacksonHandler();
        mapper.addHandler(myHandler);
        try {
            EsbServerConfigurationDto user = mapper.readValue(getClass().getResourceAsStream("/testConfig.yaml"), EsbServerConfigurationDto.class);
            System.out.println(ReflectionToStringBuilder.toString(user, ToStringStyle.MULTI_LINE_STYLE));
        } catch (Exception e) {
            fail("Error occurred when reading the yaml to dto!", e);
        }
    }

    private class MyJacksonHandler extends DeserializationProblemHandler {

    }
}
