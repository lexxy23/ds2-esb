package ds2.enterprise.esb.impl.tests;

import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.types.HttpType;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

@Dependent
public class ConfigProvider {
    @Produces
    public EsbRule<HttpType> createConfig() {
        return null;
    }
}
