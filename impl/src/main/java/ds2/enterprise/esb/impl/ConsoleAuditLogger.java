package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.impl.api.AuditLogger;
import ds2.enterprise.esb.server.api.RequestAudit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.lang.invoke.MethodHandles;

@ApplicationScoped
public class ConsoleAuditLogger implements AuditLogger {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void logAudit(RequestAudit audit) {
        LOG.info("Request done: {}", audit);
    }
}
