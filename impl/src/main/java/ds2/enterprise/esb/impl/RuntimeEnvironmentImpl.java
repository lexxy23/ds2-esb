package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.server.api.RuntimeEnvironment;

import javax.enterprise.context.ApplicationScoped;

/**
 * Created by dstrauss on 02.03.17.
 */
@ApplicationScoped
public class RuntimeEnvironmentImpl implements RuntimeEnvironment {
    @Override
    public String getVersionString() {
        return "1.0.0";
    }
    
}
