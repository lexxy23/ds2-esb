package ds2.enterprise.esb.impl.http;

import ds2.enterprise.esb.client.api.*;
import ds2.enterprise.esb.client.api.configurations.ForwardRule;
import ds2.enterprise.esb.client.api.dto.HttpWebResponseDto;
import ds2.enterprise.esb.client.api.targets.HttpTarget;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.impl.EsbRequestExecutor;
import ds2.enterprise.esb.impl.WebRequestExecutor;
import ds2.enterprise.esb.impl.api.AuditGenerator;
import ds2.enterprise.esb.impl.api.AuditLogger;
import ds2.enterprise.esb.impl.api.HttpRequestScheduler;
import ds2.enterprise.esb.impl.api.ResponseWriter;
import ds2.enterprise.esb.server.api.*;
import ds2.oss.core.api.UriTool;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by dstrauss on 27.02.17.
 */
@ApplicationScoped
public class HttpRequestSchedulerImpl implements HttpRequestScheduler {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private ExecutorService executorService;
    @Inject
    private EsbServerConfiguration configuration;
    @Inject
    @ForFlowType(FlowTypes.HTTP)
    private ResponseWriter<HttpWebResponseDto> responseWriter;
    @Inject
    @ForFlowType(FlowTypes.HTTP)
    private ConfigurationMatcher<HttpType> httpConfigurationMatcher;
    @Inject
    private HttpUrlCaller urlCaller;
    @Inject
    private ByteBufferGenerator byteBufferGenerator;
    @Inject
    private AuditGenerator<HttpType, HttpServerRequest> auditGenerator;
    @Inject
    private AuditLogger auditLogger;
    @Inject
    private HttpFilterScanner filterScanner;

    @PostConstruct
    public void onLoad() {
        executorService = Executors.newFixedThreadPool(configuration.getExecutorCount());
    }

    @PreDestroy
    public void onEnd() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(configuration.getShutdownExecutorTimeout(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.warn("Some tasks had to be killed on shutdown!", e);
        }
        LOG.debug("Shutdown done");
    }

    @Override
    public void handleRequest(IoSession session, HttpServerRequest request) throws EsbServerException, EsbRequestException {
        LOG.debug("Got a request to handleRequest in session {}: {}", session, request);
        EsbRequestExecutor esbRequestExecutor = null;
        EsbRule<HttpType> configToUse = httpConfigurationMatcher.match(request);
        if (configToUse == null) {
            throw new EsbServerException(EsbErrorCode.NO_CONFIG_MATCH, "Unknown service path");
        }
        LOG.debug("Using config {}", configToUse);
        if (configToUse.getFilters() != null) {
            //run filters before
            List<RequestFilter<HttpType, HttpServerRequest>> implList = configToUse.getFilters().stream().map(s -> filterScanner.foundFilters().get(s)).filter(f -> f != null).collect(Collectors.toList());
            for (RequestFilter<HttpType, HttpServerRequest> f : implList) {
                f.process(request);
            }
        }
        if (configToUse instanceof ForwardRule) {
            ForwardRule<HttpType> forwardConfiguration = (ForwardRule<HttpType>) configToUse;
            HttpTarget target = (HttpTarget) forwardConfiguration.getTarget();
            String targetHost = target.getTargetHost();
            int targetPort = target.getTargetPort();
            UriTool tool = UriTool.createFrom("http://" + targetHost + ":" + targetPort + request.getRequestPath());
            try {
                URL remoteUrl = tool.build().toURL();
                LOG.debug("Using URL {}", remoteUrl);
                HttpResponse extResponse = urlCaller.call(remoteUrl, request);
                EsbServerResponse<HttpType> answerForClient = null;
                HttpWebResponseDto esbResponse = new HttpWebResponseDto();
                esbResponse.setCallId(request.getCallId());
                esbResponse.setPayload(byteBufferGenerator.generate("Serving request from " + request));
                LocalDateTime doneWithRequestGetting = LocalDateTime.now();
                esbResponse.setResponseCreatedTime(doneWithRequestGetting);
                esbResponse.setResultCode(200);
                answerForClient = esbResponse;
                responseWriter.sendToClient(session, (HttpWebResponseDto) answerForClient);
                RequestAudit auditDto = auditGenerator.createAudit(request, answerForClient);
                auditLogger.logAudit(auditDto);
            } catch (MalformedURLException e) {
                throw new EsbServerException(EsbErrorCode.INTERNAL_ERROR, "Problem when building the remote url!", e);
            } catch (URISyntaxException e) {
                throw new EsbServerException(EsbErrorCode.INTERNAL_ERROR, "Problem when building the remote url!", e);
            } catch (UnsupportedEncodingException e) {
                throw new EsbServerException(EsbErrorCode.INTERNAL_ERROR, "Problem when building the remote url!", e);
            }
        } else {
            throw new EsbServerException(EsbErrorCode.NO_CONFIG_MATCH, "Unknown config, not implemented! " + configToUse.getClass().getSimpleName());
        }
        switch (request.getType()) {
            case HTTP:
                esbRequestExecutor = new WebRequestExecutor();
                break;
            default:
                throw new EsbServerException(EsbErrorCode.UNKNOWN_REQUEST_TYPE, "No idea how to handle " + request.getType());
        }
        esbRequestExecutor.setRequest(request);
        executorService.submit(esbRequestExecutor);
    }
}
