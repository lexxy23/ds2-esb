package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.server.api.HttpResponse;
import ds2.enterprise.esb.server.api.HttpServerRequest;
import ds2.enterprise.esb.server.api.HttpUrlCaller;

import javax.enterprise.context.ApplicationScoped;
import java.net.URL;

/**
 * Created by dstrauss on 02.03.17.
 */
@ApplicationScoped
public class PlainHttpUrlCallerImpl implements HttpUrlCaller {
    @Override
    public HttpResponse call(URL url, HttpServerRequest incomingRequest) {
        return null;
    }
}
