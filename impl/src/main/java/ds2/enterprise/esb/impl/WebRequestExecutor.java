package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.client.api.dto.HttpWebRequestDto;
import ds2.enterprise.esb.client.api.dto.HttpWebResponseDto;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.server.api.ForFlowType;

import javax.enterprise.context.Dependent;

/**
 * Created by dstrauss on 27.02.17.
 */
@Dependent
@ForFlowType(FlowTypes.HTTP)
public class WebRequestExecutor extends EsbRequestExecutor<HttpWebRequestDto, HttpWebResponseDto> {

    @Override
    protected HttpWebResponseDto execute() {
        return null;
    }
}
