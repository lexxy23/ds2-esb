package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.server.api.EsbServerResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.nio.ByteBuffer;
import java.time.LocalDateTime;

/**
 * Created by dstrauss on 27.02.17.
 */
@Setter
@Getter
@ToString
public class EsbRequestResponseDto implements EsbServerResponse {
    private ByteBuffer payload;
    private long payloadLength = 0;
    private String callId;
    private int resultCode;
    private LocalDateTime responseCreatedTime;

}
