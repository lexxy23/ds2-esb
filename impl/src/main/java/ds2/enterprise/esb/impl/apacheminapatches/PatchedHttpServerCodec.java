package ds2.enterprise.esb.impl.apacheminapatches;

import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.http.HttpServerDecoder;
import org.apache.mina.http.HttpServerEncoder;

/**
 * Created by dstrauss on 23.02.17.
 */
public class PatchedHttpServerCodec extends ProtocolCodecFilter{

    private static ProtocolEncoder encoder = new PatchedHttpServerEncoder();
    private static ProtocolDecoder decoder = new HttpServerDecoder();

    public PatchedHttpServerCodec(){
        super(encoder, decoder);
    }
}
