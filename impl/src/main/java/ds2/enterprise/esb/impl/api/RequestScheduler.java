package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.client.api.EsbRequestException;
import ds2.enterprise.esb.client.api.EsbServerException;
import ds2.enterprise.esb.server.api.EsbServerRequest;
import ds2.enterprise.esb.client.api.FlowType;
import org.apache.mina.core.session.IoSession;

/**
 * Created by dstrauss on 27.02.17.
 */
public interface RequestScheduler<E extends FlowType, F extends EsbServerRequest<E>> {
    /**
     * Schedules an incoming request to get serviced.
     *
     * @param session the io session
     * @param request the incoming request
     */
    void handleRequest(IoSession session, F request) throws EsbServerException, EsbRequestException;
}
