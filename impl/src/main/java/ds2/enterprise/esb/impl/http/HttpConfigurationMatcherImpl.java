package ds2.enterprise.esb.impl.http;

import ds2.enterprise.esb.client.api.EsbRule;
import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.server.api.EsbServerRequest;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.server.api.ConfigurationMatcher;
import ds2.enterprise.esb.server.api.ForFlowType;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by dstrauss on 01.03.17.
 */
@ApplicationScoped
@ForFlowType(FlowTypes.HTTP)
public class HttpConfigurationMatcherImpl implements ConfigurationMatcher<HttpType> {
    @Inject
    private EsbServerConfiguration serverConfiguration;

    @Override
    public EsbRule<HttpType> match(EsbServerRequest<HttpType> req) {
        return (EsbRule<HttpType>) serverConfiguration.getRules().get(0);
    }
}
