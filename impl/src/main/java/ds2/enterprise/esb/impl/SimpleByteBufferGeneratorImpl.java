package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.server.api.ByteBufferGenerator;

import javax.enterprise.context.Dependent;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

@Dependent
public class SimpleByteBufferGeneratorImpl implements ByteBufferGenerator {
    @Override
    public ByteBuffer generate(String plainText) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(plainText.getBytes(StandardCharsets.UTF_8));
        return byteBuffer;
    }
}
