package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.server.api.EsbServerRequest;
import ds2.enterprise.esb.server.api.EsbServerResponse;

import java.util.concurrent.Callable;

/**
 * Created by dstrauss on 27.02.17.
 */
public abstract class EsbRequestExecutor<A extends EsbServerRequest, B extends EsbServerResponse> implements Callable<EsbServerResponse> {
    protected A request;

    @Override
    public B call() throws Exception {
        B answer = execute();
        return answer;
    }

    protected abstract B execute();

    public void setRequest(A request) {
        this.request = request;
    }

}
