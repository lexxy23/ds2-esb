package ds2.enterprise.esb.impl.http;

import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.client.api.dto.HttpWebResponseDto;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.impl.api.ResponseWriter;
import ds2.enterprise.esb.server.api.ForFlowType;
import ds2.enterprise.esb.server.api.RuntimeEnvironment;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.http.DateUtil;
import org.apache.mina.http.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * For sending a give response to the client.
 */
@ApplicationScoped
@ForFlowType(FlowTypes.HTTP)
public class HttpServerREsponseWriter implements ResponseWriter<HttpWebResponseDto> {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Inject
    private RuntimeEnvironment environment;
    @Inject
    private EsbServerConfiguration serverConfiguration;

    @Override
    public void sendToClient(IoSession session, HttpWebResponseDto response) {
        synchronized (session) {
            LOG.debug("Preparing default response values..");
            Map<String, String> headers = new HashMap<>(10);
            headers.put("Content-Length", "0");
            headers.put("Content-Type", "text/plain");
            if (response.getHeaders() != null) {
                LOG.debug("Overriding response headers with the given ones..");
                //usually, this overrides anything from our defaults
                headers.putAll(response.getHeaders());
            }
            if (response.getPayloadLength() > 0) {
                headers.put("Content-Length", "" + response.getPayloadLength());
            }
            LOG.debug("Adding our signature to response..");
            headers.put("Server", "DS2 ESB Server/" + environment.getVersionString());
            headers.put("Date", DateUtil.getCurrentAsString());
            headers.put(serverConfiguration.getCallIdHeaderName(), response.getCallId());
            LOG.debug("Headers to be sent to the client so far: {}", headers);
            LOG.debug("Writing headers to client now..");
            session.write(new DefaultHttpResponse(HttpVersion.HTTP_1_1, toHttpCode(response.getResultCode()), headers));
            if (response.getPayloadLength() > 0) {
                LOG.debug("Writing payload to client..");
                session.write((HttpContentChunk) () -> Arrays.asList(response.getPayload()));
            }
            LOG.debug("Flagging end of http message to session..");
            session.write(new HttpEndOfContent());
        }
    }

    private HttpStatus toHttpCode(int httpCode) {
        for (HttpStatus status : HttpStatus.values()) {
            if (status.code() == httpCode) {
                return status;
            }
        }
        LOG.warn("Unknown unmapped http response code: {}, will use null for now", httpCode);
        return null;
    }
}
