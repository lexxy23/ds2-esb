package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.server.api.HttpServerRequest;
import ds2.enterprise.esb.client.api.types.HttpType;

/**
 * Created by dstrauss on 02.03.17.
 */
public interface HttpRequestScheduler extends RequestScheduler<HttpType, HttpServerRequest> {
}
