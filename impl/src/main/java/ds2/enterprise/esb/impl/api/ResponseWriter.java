package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.server.api.EsbServerResponse;
import org.apache.mina.core.session.IoSession;

/**
 * Created by dstrauss on 27.02.17.
 */
public interface ResponseWriter<E extends EsbServerResponse> {
    /**
     * Sends the response back to the client.
     *
     * @param session  the io session
     * @param response the response
     */
    void sendToClient(IoSession session, E response);
}
