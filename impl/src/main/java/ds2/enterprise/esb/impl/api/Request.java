package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.client.api.FlowType;

/**
 * Created by dstrauss on 27.02.17.
 */
public interface Request {
    FlowType getType();
}
