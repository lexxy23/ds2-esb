package ds2.enterprise.esb.impl.http;

import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.server.api.ForFlowType;
import ds2.enterprise.esb.server.api.HttpServerRequest;
import ds2.enterprise.esb.server.api.RequestFilter;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class HttpFilterScanner {
    @Inject
    @ForFlowType(FlowTypes.HTTP)
    private Instance<RequestFilter<HttpType, HttpServerRequest>> httpFilters;

    public Map<String, RequestFilter<HttpType, HttpServerRequest>> foundFilters() {
        Map<String, RequestFilter<HttpType, HttpServerRequest>> filterMap = new HashMap<>(2);
        httpFilters.forEach(f -> filterMap.put(f.getFilterId(), f));
        return filterMap;
    }
}
