package ds2.enterprise.esb.impl.apacheminapatches;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.http.HttpServerEncoder;
import org.apache.mina.http.api.HttpContentChunk;
import org.apache.mina.http.api.HttpEndOfContent;
import org.apache.mina.http.api.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Map;

/**
 * Created by dstrauss on 23.02.17.
 */
public class PatchedHttpServerEncoder extends HttpServerEncoder {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final CharsetEncoder ENCODER = Charset.forName("UTF-8").newEncoder();

    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
        LOG.debug("encode {}", message.getClass().getCanonicalName());
        if (message instanceof HttpResponse) {
            LOG.debug("HttpResponse");
            HttpResponse msg = (HttpResponse) message;
            String httpResponseLine="";
            StringBuilder sb=new StringBuilder(300);
            if(msg.getStatus()==null){
                sb.append("HTTP/1.1 200 OK");
                sb.append("\r\n");
            } else {
                sb.append(msg.getStatus().line());
            }
            for (Map.Entry<String, String> header : msg.getHeaders().entrySet()) {
                sb.append(header.getKey());
                sb.append(": ");
                sb.append(header.getValue());
                sb.append("\r\n");
            }
            sb.append("\r\n");
            IoBuffer buf = IoBuffer.allocate(sb.length()).setAutoExpand(true);
            buf.putString(sb.toString(), ENCODER);
            buf.flip();
            out.write(buf);
        } else if (message instanceof ByteBuffer) {
            LOG.debug("Body to send: {}", message);
            out.write(message);
        } else if(message instanceof HttpContentChunk){
            LOG.debug("Using patched content chunk encoder..");
            //parse it to use the IoBuffer instead
            HttpContentChunk chunk= (HttpContentChunk) message;
            chunk.getContent().forEach(byteBuffer -> {
                if(byteBuffer!=null){
                    IoBuffer buf = IoBuffer.wrap(byteBuffer).setAutoExpand(true);
                    //buf.flip();
                    out.write(buf);
                }
            });
        }
        else if (message instanceof HttpEndOfContent) {
            LOG.debug("End of Content");
            // end of HTTP content
            // keep alive ?
        } else {
            LOG.warn("No idea how to handle this message: {}", message.getClass().getCanonicalName());
        }

    }
}
