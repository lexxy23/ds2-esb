package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.server.api.CallIdGenerator;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

/**
 * Created by dstrauss on 06.03.17.
 */
@ApplicationScoped
public class CallIdGeneratorImpl implements CallIdGenerator {
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
