package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.server.api.EsbServerRequest;
import ds2.enterprise.esb.server.api.EsbServerResponse;

public interface AuditService {
    void audit(EsbServerRequest request, EsbServerResponse response);
}
