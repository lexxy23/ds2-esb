package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.client.api.FlowType;
import ds2.enterprise.esb.server.api.EsbServerRequest;
import ds2.enterprise.esb.server.api.EsbServerResponse;
import ds2.enterprise.esb.server.api.RequestAudit;

public interface AuditGenerator<E extends FlowType, REQ extends EsbServerRequest<E>> {
    RequestAudit createAudit(REQ request, EsbServerResponse response);
}
