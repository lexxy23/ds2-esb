package ds2.enterprise.esb.impl.http;

import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.client.api.dto.HttpWebRequestDto;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.impl.api.HttpRequestScheduler;
import ds2.enterprise.esb.server.api.CallIdGenerator;
import ds2.enterprise.esb.server.api.ForFlowType;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.http.api.HttpEndOfContent;
import org.apache.mina.http.api.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.time.LocalDateTime;

/**
 * Created by dstrauss on 17.02.17.
 */
@ApplicationScoped
@ForFlowType(FlowTypes.HTTP)
public class HttpIoRequestHandlerImpl implements IoHandler {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Inject
    private HttpRequestScheduler scheduler;
    @Inject
    private CallIdGenerator callIdGenerator;
    @Inject
    private EsbServerConfiguration configuration;

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        LOG.info("Session created: {}", session);
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        LOG.info("Session opened: {}", session);
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        LOG.debug("Session closed: {}", session);
    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        LOG.debug("Session idle: {}, {}", session, status);
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        LOG.error("Error in session {}", session, cause);
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        LOG.info("Session is {}, message received is {}", new Object[]{session, message});
        if (message instanceof HttpRequest) {
            HttpRequest externalWebRequest = (HttpRequest) message;
            HttpWebRequestDto internalRequest = new HttpWebRequestDto();
            LOG.debug("Wrapping all headers into our own request object..");
            internalRequest.setHeaders(externalWebRequest.getHeaders());
            internalRequest.setQueryParameters(externalWebRequest.getParameters());
            internalRequest.setRequestStartTime(LocalDateTime.now());
            InetSocketAddress isa = (InetSocketAddress) session.getRemoteAddress();
            internalRequest.setRequestIpAddress(isa.getHostString());
            internalRequest.setRequestPath(externalWebRequest.getRequestPath());
            internalRequest.setMethod(ds2.enterprise.esb.client.api.HttpMethod.valueOf(externalWebRequest.getMethod().name()));
            internalRequest.setExternalCallId(externalWebRequest.getHeader(configuration.getCallIdHeaderName()));
            LOG.debug("Checking for callId to use..");
            if (internalRequest.getExternalCallId() == null) {
                internalRequest.setCallId(callIdGenerator.generate());
            } else {
                internalRequest.setCallId(internalRequest.getExternalCallId());
            }
            externalWebRequest.getMethod();
            externalWebRequest.getContentType();
            externalWebRequest.getRequestPath();
            session.setAttributeIfAbsent("parsedRequest", internalRequest);
        } else if (message instanceof HttpEndOfContent) {
            HttpWebRequestDto req = (HttpWebRequestDto) session.getAttribute("parsedRequest");
            scheduler.handleRequest(session, req);
        } else {
            LOG.warn("Unknown message type: {}", message.getClass());
        }
        LOG.debug("Done with messageReceived.");
    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        LOG.debug("Session is {}, message sent is {}", new Object[]{session, message});

    }

    @Override
    public void inputClosed(IoSession session) throws Exception {
        LOG.debug("Input closed {}", new Object[]{session});
        session.closeNow();
    }
}
