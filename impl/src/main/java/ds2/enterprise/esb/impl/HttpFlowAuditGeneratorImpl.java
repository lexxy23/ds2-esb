package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.client.api.types.HttpType;
import ds2.enterprise.esb.impl.api.AuditGenerator;
import ds2.enterprise.esb.impl.api.HttpRequestAudit;
import ds2.enterprise.esb.server.api.EsbServerResponse;
import ds2.enterprise.esb.server.api.HttpServerRequest;
import ds2.enterprise.esb.server.api.RequestAudit;

import javax.enterprise.context.ApplicationScoped;
import java.time.Duration;

@ApplicationScoped
public class HttpFlowAuditGeneratorImpl implements AuditGenerator<HttpType, HttpServerRequest> {
    @Override
    public RequestAudit createAudit(HttpServerRequest request, EsbServerResponse response) {
        if (request == null) {
            return null;
        }
        HttpRequestAudit audit = new HttpRequestAudit();
        audit.setCallId(request.getCallId());
        audit.setRequestIpAddress(request.getRequestIpAddress());
        audit.setRequestStartTime(request.getRequestStartTime());
        audit.setRequestPath(request.getRequestPath());
        audit.setResultCode(response.getResultCode());
        audit.setDurationMillis(Duration.between(request.getRequestStartTime(), response.getResponseCreatedTime()).toMillisPart());
        return audit;
    }
}
