package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.server.api.RequestAudit;

public interface AuditLogger {
    void logAudit(RequestAudit audit);
}
