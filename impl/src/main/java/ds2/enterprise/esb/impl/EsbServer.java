package ds2.enterprise.esb.impl;

import ds2.enterprise.esb.client.api.EsbErrorCode;
import ds2.enterprise.esb.client.api.EsbServerConfiguration;
import ds2.enterprise.esb.client.api.EsbServerException;
import ds2.enterprise.esb.client.api.configurations.HttpForwardRule;
import ds2.enterprise.esb.client.api.connectors.HttpConnector;
import ds2.enterprise.esb.client.api.dto.HttpConnectorDto;
import ds2.enterprise.esb.client.api.types.FlowTypes;
import ds2.enterprise.esb.impl.apacheminapatches.PatchedHttpServerCodec;
import ds2.enterprise.esb.server.api.ForFlowType;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstrauss on 17.02.17.
 */
@ApplicationScoped
public class EsbServer {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Inject
    private EsbServerConfiguration configuration;
    private IoAcceptor acceptor;
    @Inject
    @ForFlowType(FlowTypes.HTTP)
    private IoHandler handler;
    private List<SocketAddress> boundAddresses;

    public EsbServer() {
        boundAddresses = new ArrayList<>(5);
    }

    public void prepare() {
        LOG.info("Preparing server..");
        acceptor = new NioSocketAcceptor();
        LOG.debug("Configuration to use is: {}", configuration);
        enlistAllHttpListeners();
    }

    @PreDestroy
    public void onShutdown() {
        stopServer();
        System.out.println("ESB Acceptor should be down now..");
    }

    public void startServer() throws EsbServerException {
        LOG.info("Starting http server..");
        try {
            acceptor.getFilterChain().addLast("logger", new LoggingFilter());
            acceptor.getFilterChain().addLast("codec", new PatchedHttpServerCodec());
            acceptor.setHandler(handler);
            acceptor.getSessionConfig().setReadBufferSize(1024);
            acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, configuration.getSocketIdleSeconds());
            acceptor.getSessionConfig().setReaderIdleTime(configuration.getReadTimeout());
            acceptor.getSessionConfig().setWriterIdleTime(configuration.getWriteTimeout());
            LOG.info("Starting possible bind..");
            acceptor.bind(boundAddresses);
            acceptor.setCloseOnDeactivation(true);
            LOG.info("Socket is online");
        } catch (IOException e) {
            LOG.error("Error when starting the server", e);
            throw new EsbServerException(EsbErrorCode.INTERNAL_ERROR, "Error when setting up the sockets.", e);
        }
    }

    private void enlistAllHttpListeners() {
        configuration.getRules().forEach(c -> {
            int typeId = c.getType().getTypeId();
            FlowTypes type = FlowTypes.valueById(typeId);
            switch (type) {
                case HTTP:
                    if (c instanceof HttpForwardRule) {
                        HttpForwardRule hfc = (HttpForwardRule) c;
                        HttpConnector cd = (HttpConnectorDto) hfc.getListener();
                        SocketAddress sa;
                        if (cd.getNetworkInterface() != null) {
                            sa = new InetSocketAddress(cd.getNetworkInterface(), cd.getListenPort());
                        } else {
                            sa = new InetSocketAddress(cd.getListenPort());
                        }
                        boundAddresses.add(sa);
                    }
                    break;
                default:
                    LOG.warn("Unknown type: {}", type);
            }
        });
    }

    public void stopServer() {
        LOG.info("Unbinding socket..");
        acceptor.unbind(boundAddresses);
    }
}
