package ds2.enterprise.esb.impl.api;

import ds2.enterprise.esb.server.api.RequestAudit;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "callId")
public class HttpRequestAudit implements RequestAudit {
    private String callId;
    private String requestIpAddress;
    private int resultCode;
    private LocalDateTime requestStartTime;
    private int durationMillis;
    private String requestPath;
    private String requestProtocol;
    private String requestServer;
    private String esbServerId;
    private String esbServerAddress;
    private long resultSize;
}
