# DS/2 ESB Server

A dummy filtering web server.


## How to build

We use gradle to build the server. So, simply run:

    ./gradlew clean build

## Test

Use the terminal:

    curl -I localhost:8082/

Using -I gives you the headers returned by the ESB query.